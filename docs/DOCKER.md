Installing Docker on Mac
------------------------

1.  Download Docker Desktop for Mac from the official Docker website: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
2.  Open the downloaded `.dmg` file and drag the Docker application to the Applications folder.
3.  Open Docker from the Applications folder.
4.  Sign in with your Docker Hub credentials (if you don't have an account, sign up at [https://hub.docker.com/](https://hub.docker.com/))
5.  That's it! You can now use Docker on your Mac.

Installing Docker on Windows
----------------------------

1.  Download Docker Desktop for Windows from the official Docker website: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
2.  Run the downloaded `.exe` file and follow the installation instructions.
3.  Sign in with your Docker Hub credentials (if you don't have an account, sign up at [https://hub.docker.com/](https://hub.docker.com/))
4.  That's it! You can now use Docker on your Windows.

Installing Docker on Linux
--------------------------

The installation of Docker on Linux varies depending on the distribution you use. On the official Docker website ([https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)), you can find installation instructions for several Linux distributions.

Here are some commonly used Docker commands:

*   `docker run`: runs a container from an image
*   `docker build`: creates an image from a Dockerfile
*   `docker ps`: lists running containers
*   `docker stop`: stops a running container
*   `docker rm`: removes a container
*   `docker images`: lists downloaded or created images
*   `docker rmi`: removes an image

If you need additional help, you can refer to the official Docker documentation at [https://docs.docker.com/](https://docs.docker.com/).