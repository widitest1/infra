#!/bin/bash

INFRA_PROJECT_DIR=$PWD
NO_COMPOSER=0

if [[ " $* " =~ " --no-composer " ]]; then
  NO_COMPOSER=1
fi

install_repositories() {
    packages=("url-shortener")
    for package in "${packages[@]}"; do
        cd ..
        if [ ! -d "$package" ]; then
            git clone "git@gitlab.com:widitest1/$package.git"
        fi
        if [ "$NO_COMPOSER" -eq 0 ]; then
            cd "$package" && composer install
        fi
        cd $INFRA_PROJECT_DIR
    done
}


cp_env() {
  packages=("url-shortener")
  for package in "${packages[@]}"; do
    docker-compose exec $package sh -c "if [ ! -f .env ]; then cp .env.example .env && php artisan key:generate; fi"
  done
}

case $1 in
    "install")
        install_repositories
        cp_env
        ;;
    "build")
        install_repositories
        cp_env
        docker-compose up -d --build
        ;;
    "destroy")
        docker rm -f $(docker ps -a -q); docker volume rm $(docker volume ls -q)
        ;;
    "up")
        install_repositories
        cp_env
        docker-compose up -d
        ;;
    "down")
        docker-compose down
        ;;
    *)
        echo "Use: h [opción]"
        echo "Options:"
        echo "  install ........  Clones front and back repositories one level above this file."
        echo "  build   ........  Create all necessary Docker containers and volumes and start all instances in background mode."
        echo "  up      ........  Starts all environments and services"
        echo "  down    ........  Stops all environments and services"
        echo "  destroy ........  Removes all containers and volumes"
        echo "  NOTE    ........  Use --no-composer flag to avoid composer install"
        ;;
esac