Install Docker on Mac/Windows/Linux.
--------------------------
[How to install docker](docs%2FDOCKER.md)

Install Composer on Mac/Windows/Linux.
--------------------------
[How to install composer](docs%2FCOMPOSER.md)

Use the command `h`.
--------------------------
With the `h` command you have the main actions to be able to run the project.

Run ./h in your console to read the description of each command.

Run `./h up` to run create/run images

Run `./h install` this will clone the rest of the repositories in the right place.

Docker services available.
--------------------------
* url-shortener-nginx: It is an Nginx web server that serves the application's backend. The URL to access this service is http://localhost:85.

* url-shortener: It is the application's backend service. It doesn't have a public URL, it can only be accessed through the Nginx web server (url-shortener-nginx).